(function () {
  angular.module('alongapp')
    .factory('Conversations', ['Restangular', function (Restangular) {
      return Restangular.withConfig(function (RestangularConfigurer) {
        RestangularConfigurer.addElementTransformer('conversations', true, function (conversation) {
          conversation.addRestangularMethod('createMessage', 'post', 'create_message');
          return conversation;
        });
      }).service('conversations');
    }])
})();