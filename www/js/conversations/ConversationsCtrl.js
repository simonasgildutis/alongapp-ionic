function ConversationsCtrl($scope, Users, CurrentUser, Restangular) {
	function getConversations() {
		Users.one(CurrentUser.id).all('conversations').getList().then(function(conversations) {
			$scope.conversations = conversations;
		});
	};

	getConversations();
};


ConversationsCtrl.$inject = ['$scope', 'Users', 'CurrentUser', 'Restangular'];
angular.module('alongapp').controller('ConversationsCtrl', ConversationsCtrl);