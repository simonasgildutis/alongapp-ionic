(function () {
  angular.module('alongapp')
    .controller('ConversationCtrl', ConversationCtrlFn)

  ConversationCtrlFn.$inject = ['$scope', 'Conversations', '$stateParams', 'CurrentUser', '$ionicScrollDelegate', '$timeout', 'Socket'];

  function ConversationCtrlFn ($scope, Conversations, $stateParams, CurrentUser, $ionicScrollDelegate, $timeout, Socket) {
    $timeout(scrollToBottom, 300);

    $scope.currentUser = CurrentUser;
    $scope.conversation = $stateParams.conversation;

    Conversations.one($stateParams.id).all('messages').getList().then(function (messages) {
      $scope.messages = messages;
    });

    $scope.postMessage = function (message) {
      Conversations.one($stateParams.id).customPOST({ text: message.text }, 'create_message').then(function () {
        message.text = '';
      });
    };

    Socket.on('conversation' + $scope.conversation.id, function (message) {
      $scope.messages.push(message);
      
      $timeout(scrollToBottom, 0);
    });

    function scrollToBottom () {
      $ionicScrollDelegate.scrollBottom(true);
    }
  }
})();