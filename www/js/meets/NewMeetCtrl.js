function NewMeetCtrl($scope, Restangular, HeaderData, $state) {
	HeaderData.set('rightPostMessage', 'postMeet', $scope);

	$scope.$on('postMeet', function() {
		if ($scope.meet.description.length > 0) {
			Restangular.all('meets').post({meet: {description: $scope.meet.description}}).then(function(meet){
				$state.go('tab.meets');
			});
		} else {
			alert("Please write something");
		}
	});
	
	$scope.meet = {
		description: ''
	};

	$scope.checkLength = function() {
		if ($scope.meet.description.length > 50) {
			$scope.meet.description = $scope.meet.description.slice(0, 50);
		};
	};
};

NewMeetCtrl.$inject = ['$scope', 'Restangular', 'HeaderData', '$state'];
angular.module('alongapp').controller('NewMeetCtrl', NewMeetCtrl);
