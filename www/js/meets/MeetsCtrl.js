function MeetsCtrl($scope, Restangular, $cordovaGeolocation, $ionicPopup) {
	function getMeets() {
		$cordovaGeolocation.getCurrentPosition({ enableHighAccuracy: true }).then(function(position) {
			return Restangular.all('meets').getList({
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			});
		}, function () {
			$ionicPopup.alert({
				title: 'Geolocation access denied',
				template: 'No meets for you then'
			});
		}).then(function (meets) {
			$scope.meets = meets;
		});
	};

	getMeets();

	$scope.doRefresh = function() {
		getMeets();
		$scope.$broadcast('scroll.refreshComplete');
	};
};


MeetsCtrl.$inject = ['$scope', 'Restangular', '$cordovaGeolocation', '$ionicPopup'];
angular.module('alongapp').controller('MeetsCtrl', MeetsCtrl);