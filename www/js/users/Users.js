(function () {
  angular.module('alongapp')
    .factory('Users', UsersFactory);

  UsersFactory.$inject = ['Restangular'];

  function UsersFactory (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.addElementTransformer('users', true, function(user) {
        user.addRestangularMethod('signIn', 'post', 'sign_in');
        return user;
      });
    }).service('users');
  }
})();