function HeaderCtrl($scope) {

	$scope.$on('rightPostMessage', function(event, data) {
		$scope.rightPost = data;
	});

	$scope.rightPostFunc = function() {
		$scope.$parent.$broadcast($scope.rightPost)
	};

};

HeaderCtrl.$inject = ['$scope'];
angular.module('alongapp').controller('HeaderCtrl', HeaderCtrl);