(function () {
  angular.module('alongapp')
    .controller('MainCtrl', MainCtrlFn);

  MainCtrlFn.$inject = ['$rootScope', '$state'];

  function MainCtrlFn ($rootScope, $state) {
    $rootScope.$on('auth-error', function () {
      $state.go('auth');
    });
  }
})();