(function () {
  angular.module('alongapp')
    .factory('Socket', ['$rootScope', function ($rootScope) {
      var socket = io.connect('http://141.136.40.194:5000/');
      return {
        on: function(eventName, callback) {
          socket.on(eventName, function() {
            var args = arguments;
            $rootScope.$apply(function() {
              callback.apply(socket, args);
            });
          });
        },
        emit: function(eventName, data, callback) {
          socket.emit(eventName, data, function() {
            var args = arguments;
            $rootScope.$apply(function() {
              if (callback) {
                callback.apply(socket, args);
              }
            });
          })
        }
      };
    }]);
})();