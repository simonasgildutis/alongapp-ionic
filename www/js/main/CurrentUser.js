(function () {
  angular.module('alongapp')
    .factory('CurrentUser', ['localStorageService', function (localStorageService) {
      return localStorageService.get('currentUser');
    }])
})();