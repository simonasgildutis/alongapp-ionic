angular.module('alongapp')
	.factory('HeaderData', [function() {
		return {
			set: function(event, data, scope) {
				scope.$parent.$broadcast(event, data);
			}
		}
	}]);