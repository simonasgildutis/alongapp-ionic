(function () {
  angular.module('alongapp')
    .factory('httpInterceptor', HttpInterceptorFn)

    HttpInterceptorFn.$inject = ['$q', 'localStorageService', '$rootScope'];

    function HttpInterceptorFn ($q, localStorageService, $rootScope) {
      return {
        request: function (config) {
          var user = localStorageService.get('currentUser');
          config.headers = config.headers || {};

          if (config.url.split('/')[3] === 'v2' && user) {
            config.headers['User-Token'] = user.authentification_key;
          }

          return config;
        },
        responseError: function (error) {
          if (error.status === 401) {
            $rootScope.$emit('auth-error');
          }

          return $q.reject(error);
        }
      };
    }
})();