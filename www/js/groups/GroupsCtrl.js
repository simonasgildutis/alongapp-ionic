function GroupsCtrl($scope, Restangular) {
	function getGroups() {
		Restangular.all('groups').getList().then(function(groups) {
			$scope.groups = groups;
		});
	};

	getGroups();
};


GroupsCtrl.$inject = ['$scope', 'Restangular'];
angular.module('alongapp').controller('GroupsCtrl', GroupsCtrl);