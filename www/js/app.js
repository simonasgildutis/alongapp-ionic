// Ionic Starter App
angular.module('alongapp', ['ionic', 'restangular', 'LocalStorageModule', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, RestangularProvider, $httpProvider, localStorageServiceProvider) {
  $httpProvider.interceptors.push('httpInterceptor');

  localStorageServiceProvider
    .setPrefix('alongapp');

  RestangularProvider.setBaseUrl('http://staging.alongapp.com/v2/');
  var headers = {};
  headers['Slaptas-Raktas'] = '109ff9032nf32okl';
  RestangularProvider.setDefaultHeaders(headers);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html",
    controller: 'MainCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.meets', {
      url: '/meets',
      views: {
        'tab-meets': {
          templateUrl: 'templates/meets/index.html',
          controller: 'MeetsCtrl'
        }
      }
    })
    .state('tab.groups', {
      url: '/groups',
      views: {
        'tab-groups': {
          templateUrl: 'templates/groups/index.html',
          controller: 'GroupsCtrl'
        }
      }
    })
    .state('tab.newMeet', {
      url: '/newMeet',
      views: {
        'tab-newMeet': {
          templateUrl: 'templates/meets/new.html',
          controller: 'NewMeetCtrl'
        }
      }
    })
  .state('tab.conversations', {
    url: '/conversations',
    views: {
      'tab-conversations': {
        templateUrl: 'templates/conversations/index.html',
        controller: 'ConversationsCtrl'
      }
    }
  })
  .state('conversation', {
    url: '/conversations/:id',
    params: {
      conversation: null
    },
    templateUrl: 'templates/conversations/show.html',
    controller: 'ConversationCtrl'
  })
  .state('tab.user', {
    url: '/user',
    views: {
      'tab-user': {
        templateUrl: 'templates/users/show.html',
        controller: 'UserCtrl'
      }
    }
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('auth', {
    url: '/login',
    controller: 'AuthCtrl',
    templateUrl: 'templates/auth/login.html'
  })

  .state('newPersonalMeet', {
    url: '/new-personal-meet',
    templateUrl: 'templates/meets/new-personal.html',
    controller: 'NewMeetCtrl'
  })
  .state('newGroup', {
    url: '/new-group',
    templateUrl: 'templates/groups/new.html',
    controller: 'NewGroupCtrl'
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/meets');

});