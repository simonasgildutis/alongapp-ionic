(function () {
  angular.module('alongapp')
    .controller('AuthCtrl', AuthCtrlFn);

  AuthCtrlFn.$inject = ['$scope', '$state', 'Users', 'localStorageService'];

  function AuthCtrlFn ($scope, $state, Users, localStorageService) {
    $scope.signIn = function (user) {
      Users.signIn(user).then(function (response) {
        localStorageService.set('currentUser', response);

        $state.go('tab.meets');
      }, function () {
        $scope.message = 'Do you even remember?'; 
      })
    };
  }
})();